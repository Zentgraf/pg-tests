#include <seqan/seq_io.h>
#include <seqan/sequence.h>

using namespace seqan;

int main()
{
    //seqan::SequenceStream seqStream("filename.fa.gz");
    CharString seqFileName = getAbsolutePath("../home/jens/PG/pg-tests/example.fa");
    CharString id;
    Dna5String seq;

    //SequenceStream seqStream(toCString(path));
    SeqFileIn seqFileIn(toCString(seqFileName));
    readRecord(id, seq, seqFileIn);
    std::cout << id << '\t' << seq << '\n';

    return 0;
}

